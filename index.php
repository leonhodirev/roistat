<?php

/*
Напиши метод, который принимает на вход строку и меняет порядок всех знаков препинания (любых небуквенных и нечисловых символов) на обратный.
Например:
$result = revertPunctuationMarks("Привет! Как твои дела?");
echo $result; // Привет? Как твои дела!
 */

function revertPunctuationMarks($text)
{
    preg_match_all('#.{1}#uis', $text, $out);
    $arrayLet = $out[0];
    $arrayPun = [];
    $newText = '';

    foreach ($arrayLet as $key => $value) {
        $bkv = $value;
        $value = preg_replace("#[[:punct:]]#", "", $value);
        if ($value == "") {
            $arrayPun[$key] = $bkv;
        }
    }
    $i = 0;
    foreach ($arrayLet as $key => $value){
        foreach ($arrayPun as $k => $item) {
            if ($key == $k) {
                $next = next($arrayPun);
                $i++;
                if ($i == count($arrayPun)) {
                    $next = array_shift($arrayPun);
                }
                $value = $next;
            }
        }
        $newText .= $value;
    }

    return $newText;
}

class RevertTest extends PHPUnit\Framework\TestCase
{
    public function testRevertPunctuationMarks()
    {
        $text = "Привет! Как твои дела?";
        $result = revertPunctuationMarks($text);
        $this->assertFalse($text == $result);
    }
}

$text = "Привет! Как твои дела?";
$result = revertPunctuationMarks($text);

echo $result; // Привет? Как твои дела!